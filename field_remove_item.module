<?php
/**
 * @file
 * Adds a checkbox remove option on each item of all multivalue fields.
 */

/**
 * Implements hook_field_widget_form_alter().
 *
 * Adds a remove option checkbox into each field item of all multivalue fields.
 */
function field_remove_item_field_widget_form_alter(&$element, &$form_state, $context) {
  // Modules such as field_collection add their own "Remove" button.
  if ($context['field']['cardinality'] == FIELD_CARDINALITY_UNLIMITED && !isset($element['remove_button']) &&
    isset($element['#type']) && !in_array($element['#type'], field_remove_item_exclude_field_types())) {
    $element['field_remove_item'] = array(
      '#type' => 'button',
      '#value' => t('Remove'),
      '#name' => drupal_html_class(
        'op-remove-' . $context['field']['field_name'] . '-' . $context['langcode'] . '-' . $context['delta']),
      '#weight' => 10,
      '#submit' => 'field_remove_item_ajax_callback',
      '#ajax' => array(
        'callback' => 'field_remove_item_ajax_callback',
      ),
      '#limit_validation_errors' => array(),
      '#field_name' => $context['field']['field_name'],
      '#langcode' => $context['langcode'],
      '#delta' => $context['delta'],
      '#field_remove_item' => TRUE,
    );
  }
}

/**
 * Do not add the 'remove' button for certain field types.
 */
function field_remove_item_exclude_field_types() {
  $types = & drupal_static(__FUNCTION__, array());

  if (empty($types)) {
    $types = array(
      'file',
      'image',
    );

    drupal_alter('field_remove_item_exclude_field_types', $types);
  }
  return $types;
}

/**
 * Ajax callback.
 */
function field_remove_item_ajax_callback(&$form, $form_state) {
  $config = _field_remove_item_ajax_remove($form, $form_state);
  $wrapper = '.' . $config['wrapper'];

  // Only render the piece of form that we want depending on the parents.
  $element = $form;
  foreach ($config['array_parents'] as $name) {
    $element = &$element[$name];
    if (!empty($element[$config['field_name']])) {
      break;
    }
  }

  return array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_replace($wrapper, render($element[$config['field_name']])),
    ),
  );
}
/**
 * Recursively look for the elements to remove in the form array.
 */
function _field_remove_item_ajax_remove(&$element, &$form_state) {
  $config = array();
  $children = element_children($element);
  if (count($children)) {
    foreach ($children as $childname) {
      if (isset($element['#field_remove_item']) && in_array($childname, array_keys($form_state['#field_remove_item']))) {
        return array(
          'array_parents' => $element['#array_parents'],
          'wrapper' => $element['#field_remove_item']['wrapper'],
          'field_name' => $element['#field_remove_item']['field_name'],
        );
      }

      $config = _field_remove_item_ajax_remove($element[$childname], $form_state);
    }
  }

  return $config;
}

/**
 * Implements hook_form_alter().
 *
 * Remove the field element.
 */
function field_remove_item_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form_state['triggering_element']['#field_remove_item'])) {
    $field_name = $form_state['triggering_element']['#field_name'];
    $language = $form_state['triggering_element']['#langcode'];
    $delta = $form_state['triggering_element']['#delta'];

    if (!isset($form_state['#field_remove_item'])) {
      $form_state['#field_remove_item'] = array();
    }

    // Keep track of this removal.
    $form_state['#field_remove_item'][$field_name][$language][] = $delta;
  }

  // Remove all items again in case the form submission fails validation.
  if (!empty($form_state['#field_remove_item'])) {
    foreach ($form_state['#field_remove_item'] as $field_name => $field_values) {
      foreach ($field_values as $language => $field_deltas) {
        foreach ($field_deltas as $delta) {
          _field_remove_item_remove_element($form, $form_state, $field_name, $language, $delta);
        }
      }
    }
  }
}

/**
 * Remove the field items recursively.
 */
function _field_remove_item_remove_element(&$element, &$form_state, $field_name, $language, $delta) {
  $children = element_children($element);
  if (count($children)) {
    foreach ($children as $childname) {
      if (is_array($element[$childname])) {
        _field_remove_item_remove_element($element[$childname], $form_state, $field_name, $language, $delta);
      }
      if ($childname === $field_name) {
        $element['#field_remove_item'] = array(
          'wrapper' => $element[$field_name]['#attributes']['class'][1],
          'field_name' => $field_name,
        );
        // Remove field item.
        unset($element[$field_name][$language][$delta]);
      }
    }
  }
}
